FROM golang:alpine3.15 AS builder

# hadolint ignore=DL3018
RUN apk add --no-cache \
        build-base \
        git \
    && git clone --depth=1 https://github.com/lindenlab/caddy-s3-proxy /src/caddy-s3-proxy

WORKDIR /src/caddy-s3-proxy

RUN go get -u github.com/caddyserver/xcaddy/cmd/xcaddy \
    && make build


FROM alpine:3.15
# hadolint ignore=DL3018
RUN apk --no-cache add ca-certificates
COPY --from=builder /src/caddy-s3-proxy/caddy /usr/bin/caddy
